//
//  AppDelegate.swift
//  Foodly
//
//  Created by Decagon on 30/05/2021.
//
import Firebase
import UIKit
import IQKeyboardManagerSwift

@main

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions
                        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        FirebaseApp.configure()
        
        // code if it's been less than 24h since last login send the user to home else send user to onboarding.
        window = UIWindow(frame: UIScreen.main.bounds)
        
        if let date = UserDefaults.standard.object(forKey: "creationTime") as? Date {
            if let diff = Calendar.current.dateComponents([.hour], from: date, to: Date()).hour, diff > 24 {
                
                let newStoryboard = UIStoryboard(name: "OnboardingStoryboard", bundle: nil)
                let newVC = newStoryboard.instantiateViewController(identifier: "OnboardingVC") as UIViewController
                window?.rootViewController = newVC
                window?.makeKeyAndVisible()
                
//            } else {
//                let newStoryboard = UIStoryboard(name: "HomeScreenStoryboard", bundle: nil)
//                let newVC = newStoryboard.instantiateViewController(identifier: "homeScreenVC") as UITabBarController
//                window?.rootViewController = newVC
//                window?.makeKeyAndVisible()
//            }
            } else {
                let newStoryboard = UIStoryboard(name: "OnboardingStoryboard", bundle: nil)
                let newVC = newStoryboard.instantiateViewController(identifier: "OnboardingVC") as UIViewController
                window?.rootViewController = newVC
                window?.makeKeyAndVisible()
                
            }
            
        } else {
            let newStoryboard = UIStoryboard(name: "OnboardingStoryboard", bundle: nil)
            let newVC = newStoryboard.instantiateViewController(identifier: "OnboardingVC") as UIViewController
            window?.rootViewController = newVC
            window?.makeKeyAndVisible()
            
        }
        
        return true
        
    }
}
