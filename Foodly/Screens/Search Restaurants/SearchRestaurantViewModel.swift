//
//  SearchRestaurantViewModel.swift
//  Foodly
//
//  Created by mac on 13/07/2021.
//

import UIKit

class SearchRestaurantViewModel{
    
    var restaurantList = [Restaurants]()
    var restaurantIDS = [String]()
    var filteredData = [Restaurants]()
    
    var closure:(() -> Void)?
    
    let restaurantsService = RestaurantService()
    
    func fetchRestaurants(categoryName: String? = nil) {
        
        restaurantsService.getRestaurants(categoryName: categoryName) { (output) in
            switch output {
            case .failure(let error):
                print(error)
            case .success(let queryDocument):
                var restaurantsFilteredArray = [Restaurants]()
                queryDocument?.documents.forEach({ (result) in
                    let resultData = result.data()
                    if let restaurantName = resultData["name"] as? String,
                       let imageName = resultData["imageName"] as? String,
                       let discount = resultData["discount"] as? String,
                       let mealType = resultData["mealType"] as? String,
                       let time = resultData["time"] as? String,
                       let restID = result.documentID as? String{
                        let eachRestaurant = Restaurants(restaurantName: restaurantName,
                                                         restaurantImage: UIImage(imageLiteralResourceName: imageName),
                                                         category: mealType,
                                                         timeLabel: time, discountLabel: discount, restaurantID: restID)
                        
                        self.restaurantIDS.append(restID)
                        restaurantsFilteredArray.append(eachRestaurant)
                    }
                    
                })
                self.restaurantList = restaurantsFilteredArray
                self.filteredData = self.restaurantList
                self.closure?()// optionally call closure
            }
        }
    }
    
     private func filterRestaurants (_ searchText: String) {
        for restaurantSearched in restaurantList {
            let restaurant = restaurantSearched.restaurantName.lowercased()
            let restaurantFood = restaurantSearched.category.lowercased()
            if restaurant.contains(searchText.lowercased()) || restaurantFood.contains(searchText.lowercased()) {
                filteredData.append(restaurantSearched)
            }
        }
        
        
    }
    
    func beginSearch(for searchText: String) {
        filteredData = []
        searchText.isEmpty ? filteredData = restaurantList : filterRestaurants(searchText)
    }
}
