//
//  SearchRestaurantViewController.swift
//  Foodly
//
//  Created by Decagon on 16/06/2021.
//

import UIKit

final class SearchRestaurantViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var restaurantTable: UITableView!
    
    lazy var refresh: UIRefreshControl = UIRefreshControl()

    private var searchViewModel = SearchRestaurantViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableSetup()
        searchBar.delegate = self
        setupViewModelListener()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    fileprivate func setupViewModelListener() {
        searchViewModel.fetchRestaurants()
        searchViewModel.closure = { [weak self] in
            DispatchQueue.main.async {
                self?.restaurantTable.reloadData()
                self?.refresh.endRefreshing()
            }
        }
    }
    
    fileprivate func tableSetup() {
        restaurantTable.register(UINib(nibName: "RestaurantsTableViewCell",
                                       bundle: nil), forCellReuseIdentifier: "cell")
        restaurantTable.dataSource = self
        restaurantTable.delegate = self
        restaurantTable.refreshControl = refresh
        refresh.addTarget(self, action: #selector(reloadRestaurant), for: .valueChanged)
        restaurantTable.addSubview(refresh)
    }
    
    @objc func reloadRestaurant() {
        searchViewModel.fetchRestaurants()
        refresh.beginRefreshing()
    }
    
    func showDetailForRestaurant(at index: Int) {
        guard let viewController = UIStoryboard(name: "Details", bundle: nil)
                .instantiateViewController(identifier: "DetailsVC")
                as? DetailsViewController else {return}
        let selectedRestaurantDetail = searchViewModel.filteredData[index]
        viewController.detailViewModel.selectedRestaurant = selectedRestaurantDetail
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension SearchRestaurantViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchViewModel.beginSearch(for: searchText)
        restaurantTable.reloadData()
    }
}

extension SearchRestaurantViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchViewModel.filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let  cell = tableView.dequeueReusableCell(withIdentifier:
                                                            "cell", for: indexPath)
                as? RestaurantsTableViewCell else {return UITableViewCell()}
        cell.setup(with: searchViewModel.filteredData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showDetailForRestaurant(at: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
