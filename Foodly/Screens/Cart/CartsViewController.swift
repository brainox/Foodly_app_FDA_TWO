//
//  CartsTableViewController.swift
//  Foodly
//
//  Created by Decagon on 16/06/2021.
//

import UIKit
import FirebaseRemoteConfig


class CartsViewController: UIViewController {
    
    @IBOutlet weak var deliveryUIView: UIView!
    @IBOutlet weak var promoUIView: UIView!
    @IBOutlet weak var continueUiView: UIButton!
    @IBOutlet weak var promoCodeTextField: UITextField!
    @IBOutlet weak var itemsTotal: UILabel!
    @IBOutlet weak var discountAmt: UILabel!
    @IBOutlet weak var taxAmt: UILabel!
    @IBOutlet weak var finalAmt: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    let cartViewModel = CartViewModel()
    var initialAmt = [Float]()
    var finalTotalAmt = [Float]()
    
    func roundedCorners() {
        promoCodeTextField.borderStyle = .none
        deliveryUIView.layer.cornerRadius = 18
        promoUIView.layer.cornerRadius = 18
        continueUiView.layer.cornerRadius = 18
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(CartTableViewCell.nib(), forCellReuseIdentifier: CartTableViewCell.identifier)
        totalAmount()
        tableView.delegate = self
        tableView.dataSource = self
        
        roundedCorners()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        totalAmount()
    }
    
    fileprivate func alertTextFields ( _ message: String) {
        let alert = UIAlertController(title: "One Moment", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
    
    fileprivate func validatePromoCode () -> Bool {
        
        let validPromoCodes = cartViewModel.promoCode.uppercased().components(separatedBy: " ")
        let inputCodes = promoCodeTextField.text?.trimmingCharacters(in: .whitespaces) ?? ""
        self.showIndicator(withTitle: "Checking promo code...") // should display for a longer time
        
        if inputCodes == "" {
            alertTextFields(Constant.invalidPromoMSG1)
            self.hideIndicator()
        }
        
        if !validPromoCodes.contains(inputCodes) {
            alertTextFields(Constant.invalidPromoMSG2)
            self.hideIndicator()
            return false
        } else {
            return true
        }
    }
    
    fileprivate func totalAmount() {
        let total = cartViewModel.filteredMeals.reduce(Double(0)) { result, meal in
            let amountOfMeal = ((Double(meal.mealPrice) ?? 0) * Double(meal.quantity)) + result
            return amountOfMeal
        }
        displayAmountDetails(with: total, and: 0, and: 0)
    }
    
    fileprivate func displayAmountDetails (with initialAmount: Double,
                                           and discount: Double,
                                           and totalDiscount: Double) {
        let tax = Double(String(format: "%.2f", ( (Double(cartViewModel.tax)! / 100) * initialAmount)))!
        itemsTotal.text = "$" + String(format: "%.2f", initialAmount)
        finalAmt.text = "$" + String(format: "%.2f", initialAmount - totalDiscount + tax)
        discountAmt.text = "$\(totalDiscount)"
        discountAmt.text = "$" + String(format: "%.2f", totalDiscount)
        taxAmt.text = "$\(tax)"
        initialAmt.removeAll()
    }
    
    fileprivate func totalAmountWithDiscount() {
        let total = cartViewModel.filteredMeals.reduce(Double(0)) { result, meal in
            let amountOfMeal = ((Double(meal.mealPrice) ?? 0) * Double(meal.quantity)) + result
            return amountOfMeal
        }
        let discount =  (Double(cartViewModel.restaurantDiscount) ?? 0) / 100
        let totalDiscount = discount * total
        displayAmountDetails(with: total, and: discount, and: totalDiscount)
    }
    
    @IBAction func promoBtnPressed(_ sender: UIButton) {
        
        if validatePromoCode() {
            self.hideIndicator()
            
            let alertController =
                UIAlertController(
                    title: "Happy Shopping!",
                    message: "Promo code is valid. Your discount from \(cartViewModel.restaurantName.capitalized) is \(cartViewModel.restaurantDiscount)% off",
                    preferredStyle: .alert)
            let acceptAction =
                UIAlertAction(title: "Accept", style: .default) {[weak self ] (_) -> Void
                    in
                    self?.hideIndicator()
                    self?.reloadCartWithDiscount()
                }
            alertController.addAction(acceptAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func continueButtonPressed(_ sender: UIButton) {
        let newStoryboard = UIStoryboard(name: "Confirmation", bundle: nil)
        let newController = newStoryboard.instantiateViewController(
            identifier: "ConfirmationViewController") as ConfirmationViewController
        newController.title = "Confirmation"
        cartViewModel.makeOrder(with: finalAmt.text)
        
        navigationController?.pushViewController(newController, animated: true)
        newController.modalTransitionStyle = .crossDissolve
        newController.modalPresentationStyle = .fullScreen
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension CartsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartViewModel.filteredMeals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell",
                                                 for: indexPath) as? CartTableViewCell
        cell?.setup(with: cartViewModel.filteredMeals[indexPath.row])
        cell?.delegate = self
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            cartViewModel.filteredMeals[indexPath.row].quantity = 0
            reloadCart()
        }
    }
}

// MARK: - CartTableViewCellDelegate
extension CartsViewController: CartTableViewCellDelegate {
        
    func addBtnTapped(sender: CartTableViewCell, on plus: Bool) {
        guard let indexForRow = tableView.indexPath(for: sender)?.row else { return }
        if plus {
          cartViewModel.filteredMeals[indexForRow].quantity += 1
        } else {
            cartViewModel.filteredMeals[indexForRow].quantity -= 1
        }
        reloadCart()
    }
    
    func reloadCart() {
        tableView.reloadData()
        totalAmount()
    }
    
    func reloadCartWithDiscount() {
        tableView.reloadData()
        totalAmountWithDiscount()
    }
    
    func minusBtnTapped(sender: CartTableViewCell, on plus: Bool) {
    }
}
