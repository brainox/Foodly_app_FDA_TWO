//
//  CartTableViewCell.swift
//  Foodly
//
//  Created by Decagon on 16/06/2021.
//

import UIKit

protocol CartTableViewCellDelegate: AnyObject {
    func addBtnTapped(sender: CartTableViewCell, on plus: Bool)
    func minusBtnTapped(sender: CartTableViewCell, on plus: Bool)
}

//protocol CartTableViewCellDelegate: AnyObject {
//    func addBtnTapped(sender: CartTableViewCell, on plus: Bool, amount: String)
//    func minusBtnTapped(sender: CartTableViewCell, on plus: Bool, amount: String)
//}


class CartTableViewCell: UITableViewCell {
    @IBOutlet private weak var restaurantImage: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var itemCountLabel: UILabel!
    @IBOutlet private weak var addButton: UIButton!
    @IBOutlet private weak var minusButton: UIButton!
    @IBOutlet private weak var priceLabel: UILabel!
    
    static let identifier = "CartTableViewCell"

    weak var delegate: CartTableViewCellDelegate?
    
    var tableViewModel = CartViewModel()
    var oldAmount = [Float]()
    override func awakeFromNib() {
        super.awakeFromNib()
        restaurantImage.layer.cornerRadius = restaurantImage.frame.width / 4
        restaurantImage.layer.borderWidth = 0.5
        restaurantImage.layer.borderColor = #colorLiteral(red: 0.6997401823, green: 0.6197884158, blue: 1, alpha: 1)
    }
    
    func setup (with model: MealsModel) {
        itemCountLabel.text = "\(model.quantity)"
        nameLabel.text = model.mealName
        priceLabel.text = "$\(Double(model.quantity) * Double(model.mealPrice)!)"
        restaurantImage.image = model.mealImage
    }
    
    static func nib () -> UINib {
        return UINib(nibName: "CartTableViewCell", bundle: nil)
    }
    
    @IBAction func toAddItem(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.addBtnTapped(sender: self, on: true)
        }
    }
    
    @IBAction func toMinusItem(_ sender: Any) {
        if let delegate = delegate {
            delegate.addBtnTapped(sender: self, on: false)
        }
    }
}
