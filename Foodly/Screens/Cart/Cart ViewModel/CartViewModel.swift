//
//  CartViewModel.swift
//  Foodly
//
//  Created by Decagon on 16/06/2021.
//

import UIKit

class CartViewModel {
//    var initialTotalAmount = [Float]()
    let getTaxAndPromoCode = RemoteConfigPromoCodes()
    var restaurantName = ""
    var restaurantDiscount = ""
    var tax = ""
    var promoCode = ""
    var selectedMeal = [MealsModel]()
    var mealsToDisplay = [MealsModel]()
    var filteredMeals: [MealsModel] {
        return mealsToDisplay.filter({$0.quantity > 0})
    }
    
    func makeOrder(with finalAmt: String?)  {
        var makeOrder = Food()
        makeOrder.name = restaurantName
        makeOrder.image = imageStringForRestaurant(check: restaurantName)
        makeOrder.items = "x \(filteredMeals.count) items"

        if let amount = finalAmt {
            makeOrder.price = amount
        }
        
        let request = OrderService()
        request.createOrder(with: makeOrder) { (result) in
            switch result {
            case .success: print("")
            case .failure(let error): print(error.localizedDescription)
            }
        }
    }
    
    private func imageStringForRestaurant (check image: String) -> String {
        switch image {
        case "Conrad food":
            return "Group 43"
        case "Mr Biggs":
            return "Group 43"
        case "Mama Nkechi":
            return "Group 45"
        case "Black Fish":
            return "Group 45"
        case "Goichi Oniko":
            return "Group 46"
        case "SK Restro":
            return "Group 44"
        default:
        return "Group 43"
        }
    }

}
