//
//  OrderHistoryViewController.swift
//  Foodly
//
//  Created by Decagon on 23.6.21.
//

import UIKit

class OrderHistoryViewController: UIViewController {
    
    @IBOutlet weak var oderHistoryTableView: UITableView!
    let orderHistoryModel = OrderHistoryModel()
    lazy var refreshOrderHistory = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadOrderHistory()
        orderHistoryModel.fetchOrderHistory()
        setUpTableView()
        self.navigationItem.title = "My Order History"
    }
    
    fileprivate func setUpTableView() {
        let nib = UINib(nibName: OrderHistoryTableViewCell.identifier, bundle: nil)
        oderHistoryTableView.register(nib, forCellReuseIdentifier: OrderHistoryTableViewCell.identifier)
        oderHistoryTableView.delegate = self
        oderHistoryTableView.dataSource = self
        oderHistoryTableView.addSubview(refreshOrderHistory)
        refreshOrderHistory.addTarget(self, action: #selector(beginRefreshOrderHistory), for: .valueChanged)
    }
    
    @objc func beginRefreshOrderHistory() {
        orderHistoryModel.fetchOrderHistory()
        refreshOrderHistory.beginRefreshing()
    }
    func reloadOrderHistory() {
        orderHistoryModel.orderHistoryCompletion = {[weak self] in
            DispatchQueue.main.async {
                self?.oderHistoryTableView.reloadData()
            }
            self?.refreshOrderHistory.endRefreshing()
        }
    }

}

extension OrderHistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderHistoryModel.orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = oderHistoryTableView.dequeueReusableCell(withIdentifier: OrderHistoryTableViewCell.identifier,
                                                                  for: indexPath)
                as? OrderHistoryTableViewCell else {fatalError()}
        cell.configure(with: orderHistoryModel.orders[indexPath.row])
        return cell
    }
}

extension OrderHistoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
}
