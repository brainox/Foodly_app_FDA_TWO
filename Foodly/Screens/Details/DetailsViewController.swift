//
//  DetailsViewController.swift
//  Foodly
//
//  Created by mac on 09/06/2021.
//

import UIKit

class DetailsViewController: UIViewController {
    
    let detailViewModel = DetailsViewModel()
    
    @IBOutlet weak var numberOfItems: UILabel!
    @IBOutlet weak var viewCartButton: UIButton!
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = detailViewModel.selectedRestaurant?.restaurantName
        tabBarController?.tabBar.isHidden = true
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        setUpTable()
        detailViewModel.getRestaurantMeals()
        updateCartView()
        detailViewModel.completed = { [weak self] in
            self?.tableView.reloadData()
        }
        detailViewModel.remoteConfig.fetchValue()
    }
    
    private func setUpTable() {
        tableView.register(DetailsTableViewCell.nib(), forCellReuseIdentifier: DetailsTableViewCell.identifier)
        tableView.register(ImageTableViewCell.nib(), forCellReuseIdentifier: ImageTableViewCell.identifier)
        tableView.register(MenuTableViewCell.nib(), forCellReuseIdentifier: MenuTableViewCell.identifier)
    }
    
    @IBAction func viewCartButton(_ sender: UIButton) {
        let cartStoryboard = UIStoryboard(name: "cart", bundle: nil)
        let cartViewController = cartStoryboard
            .instantiateViewController(identifier: "CartsViewController") as CartsViewController
        cartViewController.cartViewModel.mealsToDisplay = detailViewModel.mealList
        cartViewController.cartViewModel.selectedMeal = detailViewModel.mealList
        cartViewController.cartViewModel.restaurantName = detailViewModel.restaurantName
        cartViewController.cartViewModel.restaurantDiscount = detailViewModel.restaurantDiscount
        cartViewController.cartViewModel.tax = detailViewModel.remoteConfig.taxValue
        cartViewController.cartViewModel.promoCode = detailViewModel.remoteConfig.promoCodes
    
        navigationController?.pushViewController(cartViewController, animated: true)
    }
}

extension DetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if detailViewModel.mealList.count > 5 {
            return 5
        } else {
            return detailViewModel.mealList.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell
                                                        .identifier) as? ImageTableViewCell else {return UIView()}
        self.tableView.tableHeaderView = cell
        cell.delegate = self
        if let unwrappedDetail = detailViewModel.selectedRestaurant {
            cell.setUp(with: unwrappedDetail)
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailsTableViewCell
                                                        .identifier, for: indexPath) as?
                DetailsTableViewCell else {
            return UITableViewCell()
        }
        cell.setup(with: detailViewModel.mealList[indexPath.row])
        cell.tag = indexPath.row      
        cell.delegate = self
        return cell
    }
}

extension DetailsViewController: DetailsTableViewCellDelegate {
    func didTapAddBtn(index: Int) {
        let selectedMeal = detailViewModel.mealList[index]
        if selectedMeal.quantity > 0 {
            selectedMeal.quantity = 0
        } else {
            selectedMeal.quantity = 1
        }
        updateCartView()
        tableView.reloadData()
    }
    
    func updateCartView() {
        if detailViewModel.calcMealsPrice().1 > 1 {
            numberOfItems.text = "\(detailViewModel.calcMealsPrice().1) Items"
        } else {
            numberOfItems.text = "\(detailViewModel.calcMealsPrice().1) Item"
        }
        totalAmount.text = "$\(String(format: "%.2f", detailViewModel.calcMealsPrice().0))"
        cartView.isHidden = detailViewModel.calcMealsPrice().0 <= Double(0)
    }
}

extension DetailsViewController: ImageTableViewCellDelegate {
    func seeMore(with title: String) {
        let moreDetailsMeals = detailViewModel.mealList
        let restaurantName = detailViewModel.selectedRestaurant
        let newStoryboard = UIStoryboard(name: "MoreDetails", bundle: nil)
        guard let moreDetailsVC = newStoryboard
                .instantiateViewController(identifier: "MoreDetailsVC") as? MoreDetailsViewController else {
            return
        }
        moreDetailsVC.moreDetailsDelegate = self
        moreDetailsVC.moreDetailViewModel.restaurantDetail = detailViewModel.mealList
        moreDetailsVC.moreDetailViewModel.selectedRestaurant = restaurantName
        moreDetailsVC.moreDetailViewModel.restaurantName = detailViewModel.restaurantName
        moreDetailsVC.moreDetailViewModel.restaurantDiscount = detailViewModel.restaurantDiscount
//        moreDetailsVC.moreDetailViewModel.tax = detailViewModel.tax
//        moreDetailsVC.moreDetailViewModel.promoCode = detailViewModel.promoCode
        navigationController?.pushViewController(moreDetailsVC, animated: true)
    }
}

extension DetailsViewController: UpdateDetailsRestaurantListDelegate {
    func updateMeals() {
        updateCartView()
        tableView.reloadData()
    }
}
