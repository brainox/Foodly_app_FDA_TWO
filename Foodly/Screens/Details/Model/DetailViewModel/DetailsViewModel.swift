//
//  ViewModel.swift
//  Foodly
//
//  Created by mac on 10/06/2021.
//

import UIKit
import Firebase

class DetailsViewModel {
    var tax = ""
    var promoCode = ""
    var restaurantName = ""
    var restaurantDiscount = ""
    var mealList = [MealsModel]()
    var selectedRestaurant: Restaurants?
    let remoteConfig = RemoteConfigPromoCodes()
    
    var completed: (() -> Void)?
    
    func calcMealsPrice() -> (Double, Int) {
        var mealsQuantity = 0
        let reducedPrice =  mealList.reduce(Double(0)) { res, mealsModel in
            let result = res + ((Double(mealsModel.mealPrice) ?? Double(0)) * Double(mealsModel.quantity))
            mealsQuantity += mealsModel.quantity > 0 ? 1 : 0
            return (result)
        }
        return (reducedPrice, mealsQuantity)
    }
    
    func getRestaurantMeals() {
        let mealService = MealService()
        guard let restID = selectedRestaurant?.restaurantID else {return}
        mealService.getMeals(restaurantId: restID) {(result) in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let querySnapshot):
                var mealList = [MealsModel]()
                querySnapshot?.documents.forEach({ (result) in
                    let data = result.data()
                    if let mealName = data["name"] as? String,
                       let mealImageName = data["imageName"] as? String,
                       let mealRecipe = data["recipe"] as? String,
                       let mealPrice = data["price"] as? String {
                        guard let newPrice = Double(mealPrice) else { return }
                        let mealNewPrice = String(newPrice / 100.0)
                        let eachMeal = MealsModel(mealName: mealName,
                                                  mealImage: UIImage(imageLiteralResourceName: mealImageName),
                                                  mealPrice: mealNewPrice, mealRecipe: mealRecipe)
                        mealList.append(eachMeal)
                    }
                })
                self.mealList = mealList
                self.completed?()
            }
        }
    }
}
