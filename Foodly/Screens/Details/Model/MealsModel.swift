//
//  MealsModel.swift
//  Foodly
//
//  Created by Decagon on 23/06/2021.
//

import UIKit

class MealsModel {
    let mealName: String
    let mealImage: UIImage
    var mealPrice: String
    let mealRecipe: String
    var quantity: Int = 0
 
    init(mealName: String, mealImage: UIImage, mealPrice: String, mealRecipe: String) {
        self.mealName = mealName
        self.mealImage = mealImage
        self.mealPrice = mealPrice
        self.mealRecipe = mealRecipe
    }
}
