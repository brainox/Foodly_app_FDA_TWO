//
//  MoreDetailViewModel.swift
//  Foodly
//
//  Created by Decagon on 22/06/2021.
//

import UIKit
class MoreDetailsViewModel {
    var tax = ""
    var promoCode = ""
    var restaurantName = ""
    var restaurantDiscount = ""
    var restaurantDetail: [MealsModel] = []
    var selectedRestaurant: Restaurants?
    let remoteConfig = RemoteConfigPromoCodes()
    
    
    func calcMealsPrice() -> (Double, Int) {
        var mealsQuantity = 0
        let reducedPrice =  restaurantDetail.reduce(Double(0)) { res, mealsModel in
            let result = res + ((Double(mealsModel.mealPrice) ?? Double(0)) * Double(mealsModel.quantity))
            mealsQuantity += mealsModel.quantity > 0 ? 1 : 0
            return (result)
        }
        return (reducedPrice, mealsQuantity)
    }
    
}
