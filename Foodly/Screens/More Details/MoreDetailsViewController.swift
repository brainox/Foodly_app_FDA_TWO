//
//  MoreDetailsTableViewController.swift
//  Foodly
//
//  Created by Decagon on 22/06/2021.
//

import UIKit

protocol UpdateDetailsRestaurantListDelegate: AnyObject {
    func updateMeals()
}

class MoreDetailsViewController: UIViewController {
    
    let moreDetailViewModel = MoreDetailsViewModel()
  
    weak var moreDetailsDelegate: UpdateDetailsRestaurantListDelegate?
    
    @IBOutlet weak var numberOfItems: UILabel!
    @IBOutlet weak var viewCartButton: UIButton!
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        moreDetailViewModel.remoteConfig.fetchValue()

        tabBarController?.tabBar.isHidden = true
        setUpTable()
        updateCartView()
    }
    
    private func setUpTable() {
        title = moreDetailViewModel.selectedRestaurant?.restaurantName
        tableView.register(DetailsTableViewCell.nib(), forCellReuseIdentifier: DetailsTableViewCell.identifier)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        updateDetailsMeals()
        
    }
    
    @objc func updateDetailsMeals() {
        moreDetailsDelegate?.updateMeals()
    }
    
    @IBAction func viewCartButton(_ sender: UIButton) {
        let cartStoryboard = UIStoryboard(name: "cart", bundle: nil)
        let cartViewController = cartStoryboard
            .instantiateViewController(identifier: "CartsViewController") as CartsViewController
        cartViewController.cartViewModel.mealsToDisplay = moreDetailViewModel.restaurantDetail
        cartViewController.cartViewModel.selectedMeal = moreDetailViewModel.restaurantDetail
        cartViewController.cartViewModel.restaurantName = moreDetailViewModel.restaurantName
        cartViewController.cartViewModel.restaurantDiscount = moreDetailViewModel.restaurantDiscount
        cartViewController.cartViewModel.tax = moreDetailViewModel.remoteConfig.taxValue
        cartViewController.cartViewModel.promoCode = moreDetailViewModel.remoteConfig.promoCodes
        navigationController?.pushViewController(cartViewController, animated: true)
    }
}

extension MoreDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moreDetailViewModel.restaurantDetail.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailsTableViewCell
            .identifier, for: indexPath) as? DetailsTableViewCell else {
            return UITableViewCell()
        }
        cell.setup(with: moreDetailViewModel.restaurantDetail[indexPath.row])
        cell.delegate = self
        cell.tag = indexPath.row
        return cell
    }
}

extension MoreDetailsViewController: DetailsTableViewCellDelegate {
    func didTapAddBtn(index: Int) {
        let selectedMeal = moreDetailViewModel.restaurantDetail[index]
        if selectedMeal.quantity > 0 {
            selectedMeal.quantity = 0
        } else {
            cartView.isHidden = false
            selectedMeal.quantity = 1
        }
        updateCartView()
        tableView.reloadData()
    }
    
    func updateCartView() {
        if moreDetailViewModel.calcMealsPrice().1 > 1 {
            numberOfItems.text = "\(moreDetailViewModel.calcMealsPrice().1) Items"
        } else {
            numberOfItems.text = "\(moreDetailViewModel.calcMealsPrice().1) Item"
        }
        totalAmount.text = "$\(String(format: "%.2f", moreDetailViewModel.calcMealsPrice().0))"
        cartView.isHidden = moreDetailViewModel.calcMealsPrice().0 <= Double(0)
    }
}
